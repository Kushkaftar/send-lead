package main

import (
	"send-lead/internal/service"
	"send-lead/pkg/config"
)

const dirConfigs = "configs"

func main() {
	c := config.NewConfig("defUser", dirConfigs)

	service.Run(c)
}

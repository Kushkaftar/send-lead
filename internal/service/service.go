package service

import (
	"encoding/json"
	"log"
	"send-lead/pkg/client"
	"send-lead/pkg/config"
	"send-lead/pkg/readCSV"
)

const (
	hash = "flow_uuid"
	ip   = "ip"
)

func Run(c *config.Config) {
	log.Printf("config - %+v", c)

	// init client package
	clt := client.NewClient()

	// http headers
	headers := map[string]string{"ApiKey": c.Publisher.Key}

	// read csv file, data - array maps: key - header key, value - csv value
	data := readCSV.ReadCSV("data.csv", c.Main.Delimiter)

	for i := 0; i < len(data); i++ {

		// add ip and hash to map
		leadData := data[i]

		_, ok := leadData[ip]
		if !ok {
			leadData[ip] = c.Publisher.IP
		}

		_, ok = leadData[hash]
		if !ok {
			leadData[hash] = c.Publisher.Hash
		}

		//convert data to json
		jsonData, err := json.Marshal(leadData)
		if err != nil {
			log.Printf("error convert to json - %s", err)
		}
		resp, err := clt.Post(c.Main.URL, headers, jsonData)
		if err != nil {
			log.Printf("error send lead - %s", err)
		}
		log.Println(string(jsonData))
		log.Println(string(resp.Body))
	}
}

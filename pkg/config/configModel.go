package config

type Config struct {
	Main      Main
	Publisher Publisher
}

type Main struct {
	URL       string
	Delimiter string
}

type Publisher struct {
	Key  string
	Hash string
	IP   string
}

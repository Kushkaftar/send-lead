package readCSV

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
	"text/scanner"
)

const dirCSV = "data"

func ReadCSV(fileName, delimiter string) []map[string]string {
	path := fmt.Sprintf("%s/%s", dirCSV, fileName)

	// string to rune
	var sep scanner.Scanner
	sep.Init(strings.NewReader(delimiter))
	d := sep.Next()

	// read file
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("not read file, error - %s", err)
	}
	defer file.Close()

	reader := csv.NewReader(file)
	reader.Comma = d
	record, err := reader.ReadAll()
	if err != nil {
		log.Fatalf("dont read file, error - %s", err)
	}

	var mArr []map[string]string

	for i := 1; i < len(record); i++ {
		m := make(map[string]string)

		for j := 0; j < len(record[0]); j++ {
			m[record[0][j]] = record[i][j]
		}

		mArr = append(mArr, m)
	}

	return mArr
}

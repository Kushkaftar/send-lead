package client

type Response struct {
	Headers    map[string][]string
	Body       []uint8
	StatusCode int
}

package client

import "net/http"

func headersToMap(headers http.Header) map[string][]string {
	h := make(map[string][]string)

	for name, value := range headers {
		h[name] = value
	}

	return h
}
